package ru.mirea.ivb311.lab1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	static class Token{
		public String name;
		public String regexpr;
		Token(String nameT,String regexprT){
			this.name = nameT;
			this.regexpr =regexprT;
		}
	}
    public static ArrayList<Token> allTokensList = new ArrayList(),outTokensList = new ArrayList();
    public static void main(String[] args) {
    	BufferedReader br;
    	try {
    		br = new BufferedReader(new FileReader("tok.ens"));
	         
	        String tokenName="",tokenReg="",line="";
	        while (line != null) {	        	
	            line = br.readLine();				
	            if  (line != null)tokenName = line;	
	            line = br.readLine();				
	            if  (line != null){
	            	tokenReg = line; 	
	            	allTokensList.add(new Token(tokenName,tokenReg));
	            	//System.out.println(tokenReg);
	            }
	        }
	    }catch(Exception E){E.printStackTrace();}
    	
    	Pattern pattern;
    	Matcher matcher;
    	try {
    		br = new BufferedReader(new FileReader("test.asm"));
	        String line=" ";
	        int j,i=1;
	        while (line != null) {	        	
	            line = br.readLine();
	            if  (line != null) {
	            	//System.out.println();
	            	//System.out.print(i);
	            	for(j=0;j<allTokensList.size();j++){
	            		Token t = allTokensList.get(j);
	            		pattern = Pattern.compile(t.regexpr);
	            		matcher = pattern.matcher(line);
	            		if (matcher.matches()) {
	            			outTokensList.add(t);
	            			//System.out.print(" "+ t.name+" ");
	            			}
	            		}
	            	i++;
	            	}
	            } 
	        }catch(Exception e){e.printStackTrace();}
    	}
    }
